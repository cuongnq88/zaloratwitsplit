package com.nguyen.cuong.projecttwitsplit.view.activity

import android.os.Bundle
import com.nguyen.cuong.projecttwitsplit.R
import com.nguyen.cuong.projecttwitsplit.R.id.flContainer
import com.nguyen.cuong.projecttwitsplit.view.fragment.ChatFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment(flContainer, ChatFragment.newInstance())
    }

}
