package com.nguyen.cuong.projecttwitsplit.view.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nguyen.cuong.projecttwitsplit.R
import com.nguyen.cuong.projecttwitsplit.view.adapter.ConversationMessageAdapter
import com.nguyen.cuong.projecttwitsplit.viewmodel.MessageViewModel
import com.nguyen.cuong.twitsplit_common.data.model.Message
import com.nguyen.cuong.twitsplit_common.utils.StringUtil
import com.nguyen.cuong.twitsplit_common.utils.unsafeLazy
import kotlinx.android.synthetic.main.fragment_chat.*

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/17/18 15:26
 */
class ChatFragment : BaseFragment() {

    private lateinit var adapter: ConversationMessageAdapter

    private var dataList: ArrayList<Message> = ArrayList()

    private val viewModel by unsafeLazy {
        ViewModelProviders.of(this).get(MessageViewModel::class.java)
    }

    companion object {
        fun newInstance(): ChatFragment {
            return ChatFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel?.getMessageListData()
    }

    override fun observeLiveData() {
        viewModel?.messageLiveData?.observe(this, Observer { messageList ->
            messageList?.let {
                Log.e("DEBUG", "receiver data")
                adapter.addMessageList(it)
            }
        })
    }

    private fun initView() {
        initRecyclerView()
        ivSend.setOnClickListener {
            val text = edt_input_msg_text.text.toString()
//            text = "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself."
            if (text.isNotEmpty()) {
                Log.e("DEBUG", "text.length = " + text.length)
                if (StringUtil.isValidateMessage(text, " ", 50)) {
                    edt_input_msg_text.setText("")
                    adapter.addMessage(Message(text, true))
                    recyclerView.scrollToPosition(adapter.getLastPosition())
                } else {
                    val simpleAlert = AlertDialog.Builder(context).create()
                    simpleAlert.setTitle("Alert")
                    simpleAlert.setMessage("The message contains a span of non-whitespace characters longer than 50 characters")
                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", { dialogInterface, i ->

                    })
                    simpleAlert.show()
                }
            }
        }
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = ConversationMessageAdapter(dataList)
        recyclerView.adapter = adapter
    }

}