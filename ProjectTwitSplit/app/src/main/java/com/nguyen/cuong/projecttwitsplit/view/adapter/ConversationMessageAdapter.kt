package com.nguyen.cuong.projecttwitsplit.view.adapter

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nguyen.cuong.projecttwitsplit.R
import com.nguyen.cuong.twitsplit_common.data.model.Message
import com.nguyen.cuong.twitsplit_common.utils.StringUtil
import kotlinx.android.synthetic.main.item_message_of_me_view.view.*
import kotlinx.android.synthetic.main.item_message_of_you_view.view.*

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/17/18 16:42
 */
class ConversationMessageAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder> {

    companion object {
        private const val TYPE_MESSAGE_ME = 1
        private const val TYPE_MESSAGE_YOU = 2
    }

    private var dataList: ArrayList<Message> = ArrayList()

    constructor(data: ArrayList<Message>) {
        data?.let {
            dataList.addAll(data)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_MESSAGE_ME -> {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_message_of_me_view, parent, false)
                return MessageMeHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_message_of_you_view, parent, false)
                return MessageYouHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (dataList[position].isMe) {
            true -> TYPE_MESSAGE_ME
            false -> TYPE_MESSAGE_YOU
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val message = dataList[position]
        if (holder is MessageMeHolder) {
            holder.bindData(message)
            return
        }

        if (holder is MessageYouHolder) {
            holder.bindData(message)
            return
        }
    }

    fun addMessage(message: Message?) {
        message?.let {
            dataList.add(it)
            val position = dataList.size - 1
            notifyItemInserted(position)
        }
    }

    fun addMessageList(data: ArrayList<Message>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun getLastPosition(): Int {
        return if (dataList.size > 0) (dataList.size - 1) else 0
    }

    inner class MessageMeHolder(v: View) : RecyclerView.ViewHolder(v) {

        private var view: View = v

        private var data: Message? = null

        fun bindData(data: Message) {
            this.data = data
            view.tvMessageOfMe.text = data.getSeparateMessage()
        }

    }

    inner class MessageYouHolder(v: View) : RecyclerView.ViewHolder(v) {

        private var view: View = v

        private var data: Message? = null

        fun bindData(data: Message) {
            this.data = data
            view.tvMessageOfYou.text = data.getSeparateMessage()
        }

    }
}