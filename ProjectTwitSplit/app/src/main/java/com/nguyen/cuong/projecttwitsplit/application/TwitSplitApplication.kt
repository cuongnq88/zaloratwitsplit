package com.nguyen.cuong.projecttwitsplit.application

import com.nguyen.cuong.twitsplit_common.application.BaseApplication

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/18/18 00:51
 */
class TwitSplitApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        initialize()
    }

    /**
     * initialize Library : Facebook,ImageLoader...
     */
    private fun initialize() {

    }

}