package com.nguyen.cuong.projecttwitsplit.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import io.reactivex.disposables.Disposable

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/17/18 14:33
 */
open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    val errorLiveData = MutableLiveData<Throwable>()
    var disposal: Disposable? = null

}