package com.nguyen.cuong.projecttwitsplit.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/17/18 14:32
 */
open abstract class BaseFragment : Fragment() {

    /**
     * @see Fragment.onViewCreated
     */
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
    }

    /**
     * @see Fragment.onActivityCreated
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    /**
     * @see Fragment.onDestroy
     */
    override fun onDestroy() {
        super.onDestroy()
    }

    abstract fun observeLiveData()

}