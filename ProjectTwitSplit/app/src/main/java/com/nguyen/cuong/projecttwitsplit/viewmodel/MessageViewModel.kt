package com.nguyen.cuong.projecttwitsplit.viewmodel

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.nguyen.cuong.twitsplit_common.data.model.Message
import com.nguyen.cuong.twitsplit_common.data.repository.MessageRepository
import com.nguyen.cuong.twitsplit_common.utils.unsafeLazy

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/18/18 22:32
 */
class MessageViewModel(application: Application) : BaseViewModel(application) {

    val messageLiveData: MutableLiveData<ArrayList<Message>> by unsafeLazy {
        MutableLiveData<ArrayList<Message>>()
    }

    fun getMessageListData() {
        val dataList = MessageRepository.instance.getMessageListData()
        messageLiveData.value = dataList
    }
}