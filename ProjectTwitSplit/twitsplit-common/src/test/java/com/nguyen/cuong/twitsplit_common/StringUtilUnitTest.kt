package com.nguyen.cuong.twitsplit_common

import com.nguyen.cuong.twitsplit_common.utils.StringUtil
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/18/18 13:30
 */
class StringUtilUnitTest {

    @Test
    fun splitMessageIsCorrect() {
        val content = "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself."
        val sequenceList = StringUtil.splitMessage(content, " ", 50)
        assertEquals(2, sequenceList.size)
    }

}