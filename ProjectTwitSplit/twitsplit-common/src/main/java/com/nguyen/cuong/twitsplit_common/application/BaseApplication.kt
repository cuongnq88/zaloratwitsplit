package com.nguyen.cuong.twitsplit_common.application

import android.app.Application

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/17/18 14:36
 */
open class BaseApplication : Application() {

    companion object {
        lateinit var instance: BaseApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}