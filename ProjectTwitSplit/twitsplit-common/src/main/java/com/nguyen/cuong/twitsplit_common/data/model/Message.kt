package com.nguyen.cuong.twitsplit_common.data.model

import com.nguyen.cuong.twitsplit_common.utils.StringUtil
import java.util.*

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/15/18 21:45
 */
class Message(content: String, isMe: Boolean = false) : TwitSplitAny {
    var id: Long = 1
    var content: String = content
    var isMe: Boolean = isMe

    private val sequenceList: ArrayList<String> get() = StringUtil.splitMessage(content, " ", 50)
    private var separateMessage: String = ""

    fun getSeparateMessage(): String {
        if (separateMessage.isNotEmpty()) {
            return separateMessage
        }

        val n = sequenceList.size - 1

        for (i in 0..n) {
            var sequence = if (i == 0) sequenceList[i] else "\n" + sequenceList[i]
            separateMessage += sequence
        }
        return separateMessage
    }

}