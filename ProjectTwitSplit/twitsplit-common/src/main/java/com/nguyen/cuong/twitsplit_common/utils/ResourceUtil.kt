package com.nguyen.cuong.twitsplit_common.utils

import android.content.Context
import android.support.v4.content.ContextCompat
import com.nguyen.cuong.twitsplit_common.application.BaseApplication

/**
 * RESOURCE-related utility class.
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/18/18 00:54
 */
object ResourceUtil {

    /**
     * Returns Context.
     */
    private val context: Context get() = BaseApplication.instance.applicationContext

    /**
     * Returns a color associated with a particular resource ID
     *
     * @param resId The desired resource identifier
     * @return A single color value
     */
    fun getColor(resId: Int): Int {
        return ContextCompat.getColor(context, resId)
    }

}