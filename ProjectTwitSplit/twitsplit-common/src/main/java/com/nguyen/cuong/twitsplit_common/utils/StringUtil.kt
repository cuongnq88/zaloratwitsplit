package com.nguyen.cuong.twitsplit_common.utils

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/15/18 21:51
 */
object StringUtil {

    fun splitMessage(message: String?, delimiters: String, limitSequence: Int): ArrayList<String> {
        val result: ArrayList<String> = ArrayList()
        message?.let {

            if (it.length <= limitSequence) {
                result.add(it)
                return@let
            }

            val totalSequence = (it.length / limitSequence) + 1
            val wordList = it.split(delimiters)
            val n = wordList.size - 1

            var index = 1
            var sequence = index.toString() + "/" + totalSequence

            for (i in 0..n) {
                val word = wordList[i]
                val length = sequence.length + word.length
                if (length <= limitSequence) {
                    sequence += delimiters + word
                    if (i == n) {
                        result.add(sequence)
                        return@let
                    }
                } else {
                    result.add(sequence)
                    index++
                    sequence = index.toString() + "/" + totalSequence + " " + word
                    if (i == n) {
                        result.add(sequence)
                    }
                }
            }
        }

        return result
    }

    fun isValidateMessage(message: String?, delimiters: String, limitSequence: Int): Boolean {
        message?.let {
            val wordList = it.trim().split(delimiters)
            if (wordList.isEmpty()) {
                return false
            }

            if (wordList.size == 1 && (wordList[0].length > limitSequence)) {
                return false
            }

            for (word in wordList) {
                if (word.length > limitSequence) {
                    return false
                }
            }

            return true
        }
        return false
    }

}