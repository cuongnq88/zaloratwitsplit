package com.nguyen.cuong.twitsplit_common.data.repository

import com.nguyen.cuong.twitsplit_common.data.model.Message
import com.nguyen.cuong.twitsplit_common.data.model.PostResponse
import com.nguyen.cuong.twitsplit_common.data.service.ApiService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/15/18 21:54
 */
class MessageRepository(private val apiService: ApiService) {

    companion object {
        val instance: MessageRepository by lazy { MessageRepository(ApiService.create()) }
    }

    fun postMessage(message: Message): Single<PostResponse> {
        return apiService.postMessage(message.content)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getMessageListData(): ArrayList<Message> {
        var dataList: ArrayList<Message> = ArrayList()
        dataList.add(Message("Hello"))
        dataList.add(Message("Hi", true))
        dataList.add(Message("How are you?"))
        dataList.add(Message("I'm fine. Thanks, and you.", true))
        dataList.add(Message("I'm good. Thanks"))
        return dataList
    }
}