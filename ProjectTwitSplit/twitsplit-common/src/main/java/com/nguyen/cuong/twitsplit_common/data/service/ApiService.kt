package com.nguyen.cuong.twitsplit_common.data.service

import com.nguyen.cuong.twitsplit_common.BuildConfig.DEBUG
import com.nguyen.cuong.twitsplit_common.data.model.PostResponse
import com.nguyen.cuong.twitsplit_common.data.model.User
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


/**
 * API is a client class.
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/15/18 21:53
 */
interface ApiService {

    companion object {
        private const val baseUrl = "https://api.github.com/"
        private val retrofit: ApiService by lazy {
            Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(createOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build().create(ApiService::class.java)
        }

        private fun createOkHttpClient(): OkHttpClient {
            return when (DEBUG) {
                true -> {
                    val interceptor = HttpLoggingInterceptor()
                    interceptor.level = HttpLoggingInterceptor.Level.BODY
                    return OkHttpClient.Builder()
                            .readTimeout(30, TimeUnit.SECONDS)
                            .connectTimeout(30, TimeUnit.SECONDS)
                            .addInterceptor(interceptor).build()
                }
                false -> OkHttpClient()
            }
        }

        fun create(): ApiService {
            return retrofit
        }
    }

    /**
     * @GET declares an HTTP GET request
     * @Path("user") annotation on the userId parameter marks it as a
     * replacement for the {user} placeholder in the @GET path
     */
    @GET("/users/{user}")
    fun getUser(@Path("user") userId: String): Single<User>

    @POST("/users/message")
    fun postMessage(@Query("content") content: String): Single<PostResponse>
}













