package com.nguyen.cuong.twitsplit_common.utils

/**
 * @author nguyenquoccuong
 * @version 1.0
 * @since 1.0 3/18/18 23:26
 */

fun <T> unsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)